#  Hip Chat Message Reponse 

This is the Android application genration the reponse with JSON format when user input the string with specifield Patterns 

![Screenshot](https://bytebucket.org/victor_nguyen/hipchatexercise/raw/47c53f7e1192a14594832f46ecda633018254f1f/device-2015-07-27-074847.png) 

## Feature
* Parse Mentions input message  "@mentions "" 
* Parse Emoticons input message  (Emoticons) maxium is 15 character 
* Parse Link input message return the webtitle base on link 

## Note
* in This Application we use the Jsoup libary to parse and get webpage title via link
* in Thia Application we use GSon to parse JSON String to Object  
* For the Testing purpose we user robotium 

## License

   Develop by https://bitbucket.org/victor_nguyen
