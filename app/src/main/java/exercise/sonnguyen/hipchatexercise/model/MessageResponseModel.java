package exercise.sonnguyen.hipchatexercise.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sonnguyen on 7/21/15.
 */
public class MessageResponseModel {
    @SerializedName("emoticons")
    private ArrayList<String> listEmoticons;

    @SerializedName("mentions")
    private ArrayList<String> listMentions;

    @SerializedName("links")
    private ArrayList<LinkModel> ListLinks;

    public ArrayList<String> getListEmoticons() {
        return listEmoticons;
    }

    public void setListEmoticons(ArrayList<String> listEmoticons) {
        this.listEmoticons = listEmoticons;
    }

    public ArrayList<String> getListMentions() {
        return listMentions;
    }

    public void setListMentions(ArrayList<String> listMentions) {
        this.listMentions = listMentions;
    }

    public ArrayList<LinkModel> getListLinks() {
        return ListLinks;
    }

    public void setListLinks(ArrayList<LinkModel> listLinks) {
        ListLinks = listLinks;
    }
}
