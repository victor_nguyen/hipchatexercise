package exercise.sonnguyen.hipchatexercise.model;

/**
 * Created by sonnguyen on 7/21/15.
 * this is the link model ,
 */
public class LinkModel {
    private String url;
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
