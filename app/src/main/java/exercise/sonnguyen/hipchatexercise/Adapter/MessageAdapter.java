package exercise.sonnguyen.hipchatexercise.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import exercise.sonnguyen.hipchatexercise.R;

/**
 * Created by sonnguyen on 7/21/15.
 * This is adapter for list message ,
 */
public class MessageAdapter extends ArrayAdapter<String>{
    ArrayList<String> arrayListItem;

    public MessageAdapter(Context context, ArrayList<String> arrayItem) {
        super(context,0 ,arrayItem);
        arrayListItem = arrayItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.view_mesage_item, parent, false);
            viewHolder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvMessage.setText(arrayListItem.get(position));
        return convertView;
    }



    private static class ViewHolder {
        TextView tvMessage;

    }
}
