package exercise.sonnguyen.hipchatexercise.Manager;


import android.util.Log;


import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.List;

import exercise.sonnguyen.hipchatexercise.Util.PatternsUtils;
import exercise.sonnguyen.hipchatexercise.model.LinkModel;
import exercise.sonnguyen.hipchatexercise.model.MessageResponseModel;

/**
 * Created by sonnguyen on 7/21/15.
 * This is the manager handle logic data,
 * this receive the input message and return the response for user
 */

public class DataHandleManager {
    private static DataHandleManager instance;
    private static final  String TAG= DataHandleManager.class.getName();
    private DataHandleManager() {

    }
    public static DataHandleManager getInstance() {
        if (instance == null) {
            synchronized (DataHandleManager.class) {
                if (instance == null) {
                    instance = new DataHandleManager();
                }
            }
        }

        return instance;
    }
    /**
     * handleInputMessage with input Message
     *
     * This function will check the input message match with any patterns we define Emoticon, Mention or the Link
     * and return the string response
     *
     * @param messageInput        input message

     */

    public String handleInputMessage(String messageInput){
        List<String> listMentions;
        List<String> lisEmoticons;
        List<LinkModel> listLinkModel=null;
        List<String> lisLink;
        lisEmoticons = PatternsUtils.getEmoticons(messageInput);
        listMentions = PatternsUtils.getMentions(messageInput);
        lisLink= PatternsUtils.getLinks(messageInput);
        // after get the link use this link to get the title of link
        if(lisLink!=null && lisLink.size()>0){
            listLinkModel = new ArrayList<LinkModel>();
            for(String url : lisLink){
                // create linkMode
                LinkModel model = new LinkModel();
                String tile = requestWebTitle(url);
                model.setTitle(tile);
                model.setUrl(url);
                listLinkModel.add(model);
            }
        }
        // if the input string not match with any pattens return the input string
        if(isEmptyListString(lisEmoticons) && isEmptyListString(listMentions) && isEmptyListString(listLinkModel)){
            return messageInput;
        }

        // create the message response model
        MessageResponseModel messageResponseModel = new MessageResponseModel();
        if(!isEmptyListString(lisEmoticons)){
            messageResponseModel.setListEmoticons((ArrayList) lisEmoticons);
        }
        if(!isEmptyListString(listMentions)){
            messageResponseModel.setListMentions((ArrayList) listMentions);
        }
        messageResponseModel.setListLinks((ArrayList)listLinkModel);
        // Using Gson to parse the model to String Json
        String JsonResponse = new Gson().toJson(messageResponseModel,MessageResponseModel.class);
        return  JsonResponse;

    }
    /**
     * requestWebTitle with input URL
     *
     * @param url        input url

     */
    public String requestWebTitle(String url){
        String title=null;
        try {
            Document document = Jsoup.connect(url).get();
            title = document.title();
        }catch (Exception e){

           Log.e(TAG,e.getMessage());

        }
        return  title;

    }


    /**
     * isEmptyListString
     * check the list is null or empty
     *
     * @param list       list object input

     */
    private static boolean isEmptyListString(List<?> list){
        return  list== null || list.size()<=0;
    }

}
