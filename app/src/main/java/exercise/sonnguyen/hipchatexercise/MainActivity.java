package exercise.sonnguyen.hipchatexercise;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;

import exercise.sonnguyen.hipchatexercise.Adapter.MessageAdapter;
import exercise.sonnguyen.hipchatexercise.Util.CustomProgressDialogHelper;
import exercise.sonnguyen.hipchatexercise.Manager.DataHandleManager;


public class MainActivity extends ActionBarActivity {
    Gson mGson;
    Activity mActivity;
    EditText mEditMessage;
    ListView mListViewMessage;
    ImageButton btnSend;
    MessageAdapter mMessageAdapter;
    ArrayList<String> arrayMessageChat;
    CustomProgressDialogHelper mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGson= new Gson();
        mActivity= this;
        initView();
    }
    /*
        init view for activity
     */
    private void initView(){
        mEditMessage=(EditText)findViewById(R.id.editMessage);
        mListViewMessage=(ListView)findViewById(R.id.listMessageChat);
        btnSend =(ImageButton)findViewById(R.id.btnSend);

    }

    /*
        init data update on view
     */
    private void initData()
    {
        // handle event click
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleInputMessage(mEditMessage.getText().toString());
            }
        });
        // create message adapter to show user messages
        arrayMessageChat = new ArrayList<String>();
        mMessageAdapter = new MessageAdapter(mActivity,arrayMessageChat);
        mListViewMessage.setAdapter(mMessageAdapter);
        mMessageAdapter.notifyDataSetChanged();
    }



    @Override
    protected void onResume() {
        super.onResume();
        initData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//
    private class GetResponse extends AsyncTask<String,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog= CustomProgressDialogHelper.show(mActivity,"","");
        }

        @Override
        protected String doInBackground(String... strings) {
            return DataHandleManager.getInstance().handleInputMessage(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            updateDataMessageToList(s);
            dismissDialog();

        }
    }
    private void handleInputMessage (String message){
        if(message==null || message.isEmpty()){
            mEditMessage.setError(getResources().getString(R.string.text_error_message_empty));
        }else{
            // check network state
//            if(NetWorkUtils.isNetworkAvailable(mActivity)){
             GetResponse responseTask = new GetResponse();
                responseTask.execute(message);
            // remove current input text
                mEditMessage.setText("");
//            }else{
//                Toast.makeText(mActivity,getString(R.string.network_error_message),Toast.LENGTH_SHORT).show();
//            }


        }
    }
    // update add message in list
    private void updateDataMessageToList(String messageResponse){
        arrayMessageChat.add(messageResponse);
        mMessageAdapter.notifyDataSetChanged();
    }

    // dismiss dialog when it show

    private  void dismissDialog(){
        if(mProgressDialog!=null && mProgressDialog.isShowing() && !mActivity.isFinishing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }
    public ListView getListView(){
        return mListViewMessage;
    }
}
