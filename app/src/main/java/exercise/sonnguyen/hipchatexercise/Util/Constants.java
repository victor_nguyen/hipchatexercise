package exercise.sonnguyen.hipchatexercise.Util;

/**
 * Created by sonnguyen on 7/27/15.
 */
public class Constants {
    public static final String PATTERN_MENTIONS="(?<=@)([a-zA-Z]+)";//begin @, just alphabe characters, at least 1 character
    public static final String PATTERN_EMOTICONS="(?<=\\()([a-zA-Z0-9]{1,15})(?=\\))";//begin '(' , just alphabe characters, at least 1 char, no more 15 char, end ')'

}
