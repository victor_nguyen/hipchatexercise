package exercise.sonnguyen.hipchatexercise.Util;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sonnguyen on 7/27/15.
 */
public class PatternsUtils {
    private static final  String TAG= PatternsUtils.class.getName();



    /**
     * get Mentions from input String
     *
     * @param inputString       input String
     */
    public static List<String> getMentions(String inputString) {

        Pattern pattern = Pattern.compile(Constants.PATTERN_MENTIONS);
        Matcher matcher = pattern.matcher(inputString + " ");

        return getMatcherArrayStrings(matcher, inputString);
    }



    /**
     * get Emoticons from input String
     *
     * @param inputString      input String

     */
    public static List<String> getEmoticons(String inputString) {


        Pattern pattern = Pattern.compile(Constants.PATTERN_EMOTICONS);
        Matcher matcher = pattern.matcher(inputString);

        return getMatcherArrayStrings(matcher, inputString);

    }
    /**
     * get Link from input String
     *
     * @param inputString       input String

     */
    public static List<String> getLinks(String inputString) {
        Matcher matcher = android.util.Patterns.WEB_URL.matcher(inputString); // default pattern for url of Android
        return getMatcherArrayStrings(matcher, inputString);
    }





    /**
     * get List String match with Pattern Matcher from input String
     *
     * @param inputString       input String
     * @param matcher       Patterns matcher

     */
    private static List<String> getMatcherArrayStrings(Matcher matcher, String inputString ) {
        List<String> result = new ArrayList<>();

        if (!isEmptyInputString(inputString)) {
            while (matcher.find()) {
                String matchString = matcher.group();
                Log.d(TAG, "match String: " + matchString);
                result.add(matchString);
            }
        }
        return result;
    }



    /**
     * isEmptyInputString
     * check the input string is null or empty
     *
     * @param inputString       input String

     */
    private static boolean isEmptyInputString(String inputString) {
        return inputString == null || inputString.isEmpty();
    }

    /**
     * isEmptyListString
     * check the list is null or empty
     *
     * @param list       list object input

     */
    private static boolean isEmptyListString(List<?> list){
        return  list== null || list.size()<=0;
    }

}
