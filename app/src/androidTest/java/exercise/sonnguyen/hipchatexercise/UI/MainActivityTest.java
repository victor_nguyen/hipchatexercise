package exercise.sonnguyen.hipchatexercise.UI;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import exercise.sonnguyen.hipchatexercise.Adapter.MessageAdapter;
import exercise.sonnguyen.hipchatexercise.MainActivity;
import exercise.sonnguyen.hipchatexercise.R;
import exercise.sonnguyen.hipchatexercise.Utils.TestUtils;

/**
 * Created by sonnguyen on 7/27/15.
 *
 */
public class MainActivityTest extends BaseInstrumentationTestCase<MainActivity>{
    public MainActivityTest() {
        super(MainActivity.class);
    }

    // text object null
    public void testAllObjectMessageScreenValid() throws Throwable {
        assertNotNull(getSolo().getView(R.id.btnSend));
        assertNotNull(getSolo().getView(R.id.editMessage));
        assertNotNull(getSolo().getView(R.id.listMessageChat));
    }

    /*
       Test User in put empty message
    */
    public void testInputEmptyMessage()throws Throwable{
            pressSendButton(TestUtils.EMPTY_STRING);
            assertTrue(getSolo().waitForText(getSolo().getString(R.string.text_error_message_empty)));
    }


    /*
       Test InputMentions
       1.Mentions
    it will find any text match with your expectation
    */
    public void testInputMentions()throws Throwable{
        pressSendButton(TestUtils.INPUT_MENTIONS);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_MENTIONS));
    }

    /*
       Test InputEmoticons
       1.Emoticons
        it will find any text match with your expectation
        */
    public void testInputEmoticons()throws Throwable{
        pressSendButton(TestUtils.INPUT_EMOTICONS);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_EMOTICONS));
    }

    /*
        Test InputLinks
    it will find any text match with your expectation
        1.Link
     */
    public void testInputLinks() throws Throwable {
        pressSendButton(TestUtils.INPUT_LINKS);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_LINKS));
    }

    /*
        Test all input type
        1. Mentions
        2.Emoticons
        3.Link
            it will find any text match with your expectation
         */
    public void testInputAllType()throws Throwable{
        pressSendButton(TestUtils.INPUT_ALL_TYPE);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_ALL_TYPE));

    }

    /*
        Test in put free text
        it will find any text match with your expectation
     */
    public void testInputFreeText()throws Throwable{
        pressSendButton(TestUtils.INPUT_FREE_TEXT);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_FREE_TEXT));
    }

    /*
       Test in put Special text
       it will find any text match with your expectation
    */
    public void testInputSpecialCharacter()throws Throwable{
        pressSendButton(TestUtils.INPUT_SPECIAL_CHARACTER);
        assertTrue(getSolo().waitForText(TestUtils.RESPONSE_SPECIAL_CHARACTER));
    }


    /*
        Test Emotion with over 15 character
        it will find any text match with your expectation
     */
    public void testInputOver15EmoticonsCharacter()throws Throwable{
        pressSendButton(TestUtils.INPUT_OVER_15_CHARACTER);
        getSolo().sleep(500);
        checkItemFromListChatMessage(TestUtils.RESPONSE_OVER_15_CHARACTER);

    }

    // Test Click Button Send Message
    private void pressSendButton(final String inputMessage) throws Throwable {
        final EditText messageEditText = (EditText) getSolo().getView(R.id.editMessage);
        getSolo().clearEditText(messageEditText);
        getSolo().typeText(messageEditText, inputMessage);
        clickButton(R.id.btnSend);
    }

    /*
     this function use to check the string text is in listview or not
     this use in some case when we have itemview in listview have many view child
      */
    public void checkItemFromListChatMessage(String text){
       final MainActivity mainActivity = getActivity();
        final ListView mListView= mainActivity.getListView();
        assertNotNull(getChildViewByText(mListView,text));
    }
    /*
     this function use to check the string text is in listview or not
     this use in some case when we have itemview in listview have many view child
      */
    private View getChildViewByText(ListView listView,String text )
    {
        View view= null;
        int count = listView.getCount();
        for(int i=0;i<count;i++){
            RelativeLayout relativeLayout = (RelativeLayout)listView.getChildAt(i);
            TextView textView = (TextView)relativeLayout.findViewById(R.id.tvMessage);
            if(textView!=null && textView.getText().toString().equalsIgnoreCase(text)){
                view= textView;
                break;
            }
        }
        return view;

    }
}
