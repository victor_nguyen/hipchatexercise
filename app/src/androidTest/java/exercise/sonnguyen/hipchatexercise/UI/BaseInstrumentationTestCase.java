package exercise.sonnguyen.hipchatexercise.UI;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * Created by sonnguyen on 7/27/15.
 * This class contains a protected Solo object for use in unit tests
 */
public abstract class BaseInstrumentationTestCase<T extends Activity> extends ActivityInstrumentationTestCase2<T> {
    private Solo mSolo;
    public BaseInstrumentationTestCase(Class<T> activityClass) {
        super(activityClass);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        getSolo().finishOpenedActivities();
    }
    // handle call Event button click
    protected void clickButton(int id) {
        getSolo().clickOnView(getSolo().getView(id));
    }
    // get Solo
    protected Solo getSolo() {
        if (mSolo == null) {
            mSolo = new Solo(getInstrumentation(), getActivity());
        }
        return mSolo;
    }
}
