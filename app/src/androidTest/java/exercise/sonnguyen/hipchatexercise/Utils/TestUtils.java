package exercise.sonnguyen.hipchatexercise.Utils;

/**
 * Created by sonnguyen on 7/27/15.
 */
public class TestUtils {
    public static final String EMPTY_STRING = "";
    public static final String INPUT_EMOTICONS = "Good morning! (megusta) (coffee) ";
    public static final String RESPONSE_EMOTICONS = "{\"emoticons\":[\"megusta\",\"coffee\"]}";

    public static final String INPUT_MENTIONS = "@chris you around? ";
    public static final String RESPONSE_MENTIONS = "{\"mentions\":[\"chris\"]}";

    public static final String INPUT_LINKS= "Olympics are starting soon; http://www.nbcolympics.com";
    public static final String RESPONSE_LINKS = "{\"links\":[{\"title\":\"NBC Olympics | Home of the 2016 Olympic Games in Rio\",\"url\":\"http://www.nbcolympics.com\"}]}";

    public static final String INPUT_ALL_TYPE= "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016 ";
    public static final String RESPONSE_ALL_TYPE = "{\"links\":[{\"title\":\"Justin Dorfman on Twitter: \\\"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\\\"\",\"url\":\"https://twitter.com/jdorfman/status/430511497475670016\"}],\"emoticons\":[\"success\"],\"mentions\":[\"bob\",\"john\"]}";


    public static final String INPUT_FREE_TEXT = "this his hipchat demo chat message not match any pattern()";
    public static final String RESPONSE_FREE_TEXT = "this his hipchat demo chat message not match any pattern()";

    public static final String INPUT_SPECIAL_CHARACTER= "%^&(*()";
    public static final String RESPONSE_SPECIAL_CHARACTER = "%^&(*()";

    public static final String INPUT_OVER_15_CHARACTER= "chat message not match any pattern (afskfjaksjjahnsagsaggds)";
    public static final String RESPONSE_OVER_15_CHARACTER ="chat message not match any pattern (afskfjaksjjahnsagsaggds)";
}
